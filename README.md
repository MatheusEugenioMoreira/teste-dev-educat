# Teste Dev Full Stack (Geral)

## **Índice**

- <a href="#❓-sobre-o-projeto">Sobre</a>&nbsp;&nbsp;&nbsp;
- <a href="#💻-tecnologias">Tecnologias</a>&nbsp;&nbsp;&nbsp;
- <a href="#📦-instalação-e-usabilidade">Instalação e usabilidade</a>&nbsp;&nbsp;&nbsp;
  - <a href="#pré-requisitos">Pré-requisitos</a>&nbsp;&nbsp;
  - <a href="#instalação">Instalação</a>&nbsp;&nbsp;

---

## **❓ Sobre o projeto**

## Este projeto possui o intuito de cria um sistema de frequencia de aulas, podendo ser adicionado diversos alunos em cada disciplina e podendo dar ou não a presença, sem ter a necessidade de salvar as alterações ou atualizar a página.

## **💻 Tecnologias**

### As seguintes tecnologias foram utilizadas no desenvolvimento do projeto:

- [ReactJS](https://reactjs.org/)
- [JavaScript](https://www.javascript.com/)
- [Python](https://www.python.org/)
- [Django](https://www.djangoproject.com/)

---

## **📦 Instalação e usabilidade**

### **Pré-requisitos**

- #### **Você deverá ter instalado**
  - **[Git](https://git-scm.com/)**
  - **[Node.js](https://nodejs.org/en/)**
  - **[Npm](https://www.npmjs.com/)** ou **[Yarn](https://yarnpkg.com/)**.
  - **[Python3](https://python.org/)**
  - **[Pyenv](https://klauslaube.com.br/2016/04/26/o-simples-e-poderoso-pyenv.html)**
  - **[Virtualenvwrapper](https://klauslaube.com.br/2016/04/26/o-simples-e-poderoso-pyenv.html)**
  - **[Pip](https://pypi.org/)**
  - **[Docker](https://www.docker.com/products/docker-desktop)**
  - **[Imagem do SQL no Docker](https://docs.microsoft.com/pt-br/sql/linux/quickstart-install-connect-docker?view=sql-server-ver15&pivots=cs1-bash)**
  - **[Visual Studio Code](https://code.visualstudio.com/docs/?dv=osx)**
  - **[Azure Data Studio](https://go.microsoft.com/fwlink/?linkid=2168435)**

### **Instalação**

---

## **GERAL**

#### Depois de instalar todos os pré-requisitos na sua máquina, siga este passo a passo, para ter acesso ao sistema.

#### **Clone o repositório do sistema**

```bash
$ git clone https://gitlab.com/r13/educat/NOME_DO_PROJETO_NO_GITLAB.git
```

#### Acesse o repositório clonado e crie um ambiente virtual do Python para este projeto seguindo o passo a passo abaixo:

---

#### **Instale primeiramente o `virtualenvwrapper` em sua máquina usando `pip`.**

#### **Em sequência ative-o em seu shell usando `pyenv`:**

```bash
$ pyenv virtualenvwrapper
```

#### **e para cria-lo use o comando:**

```
 $ pyenv shell <versão do seu python>
```

#### **Dê um nome para o ambiente criado.**

---

## **BACK-END**

#### Agora que criou o ambiente virtual, poderá acessar a pasta do Back-end

#### **Após iniciar seu ambiente:**

```bash
$ workon <nome escolhido (Ex: venv)>
```

### **Instale as dependências necessárias:**

```bash
$ pip install -r requirements.txt
```

---

#### Agora, deve-se criar as tabelas no banco de dados com o comando:

```bash
$ python manage.py migrate
```

#### **Crie um super usuário e sua senha para que consiga prosseguir no menu de admin:**

```bash
$ python manage.py createsuperuser
```

#### **Rode o projeto.**

```bash
$ python manage.py runserver
```

#### **Deve-se aparecer estas linhas em seu terminal:**

```bash
(...)
System check identified no issues (0 silenced).
October 12, 2021 - 01:46:48
Django version 3.0.5, using settings 'api.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

## _🔥 O Back-end estará rodando na porta 8000 🔥_

---

## **FRONT-END**

#### **Para rodar o Front-end do projeto**:

#### Acesse a pasta **ui** pelo shell e instale a depedência `yarn` ou `npm`, logo após a inicie para rodar o projeto:

## _🔥 O Front-end estará rodando na porta 3000 🔥_

---

### Este projeto foi desenvolvido por Matheus Eugenio Moreira - 2021
